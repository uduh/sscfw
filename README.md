# Numpy helper library

Library for simplifying your numpy/matplotlib workflow. Includes easier setup,
plotting and data caching. Focuses on simplicity and producing deterministic
rerunnable scripts.

All features are included in the
[general](https://uduh.gitlab.io/numpy_helper/numpy_helper.html) submodule which is
imported by default when importing `numpy_helper`.

---

## Table of contents

- [Installation](#installation)
- [Usage example](#usage-example)
    - [Other examples](#other-examples)
- [Documentation](#documentation)
- [Roadmap](#roadmap)

## Installation

1. Clone the repository: `git clone https://gitlab.com/uduh/numpy_helper.git`
2. Move into the cloned repository: `cd numpy_helper`
3. Install with pip: `pip install .`

If you want to upgrade the existing installation, use `pip install --upgrade .`
instead.

**Dependencies** (installed with pip automatically):
- [numpy](https://github.com/numpy/numpy)
- [matplotlib](https://github.com/matplotlib/matplotlib)

## Usage example

In scientific computing, there are typically 2 main steps involved: calculating
something and plotting it. This framework simplifies 2 additional practices that
are often used:
- Sometimes, calculations take a long time, so it is convenient to cache them.
- Plots should sometimes be shown and sometimes exported (saved).

A general workflow using the framework consists of the following steps:
1. Write a function that calculates something, returns the results in numpy
   ndarrays and decorate it with `@cache_numpys`.
2. Write a function that plots the calculated data, returns the matplotlib
   Figure objects and decorate it with `@handle_plots`.
3. Wrap the calls to above functions in another function (think of it as the
   `main` function in many programming languages) and pass it to `run` function.
4. Call the script (e.g.: `python my_script.py`). Use the `-e` flag to export
   all the figures to files (e.g.: `python my_script.py -e`). Use the `-r` flag
   to recompute all the cached data that is needed (e.g.: `python my_script.py
   -e`). This might be useful if the function from 1. is changed.

By default, data gets cached in data/ directory, figures get exported to
images/ directory. See the
[documentation](https://uduh.gitlab.io/numpy_helper/numpy_helper.html) for more
information.

A minimal example (also accessible at
[general_pattern.py](https://gitlab.com/uduh/numpy_helper/-/blob/main/examples/general_pattern.py))
is shown below:

```python
import numpy_helper as nh
import numpy as np
import matplotlib.pyplot as plt


@nh.cache_numpys(num_of_returns=2)
def some_data(a, b=2):
    x = np.linspace(0, 2 * np.pi, 1000)
    y = b * np.sin(a * x)

    return x, y


@nh.handle_plot
def plot_some_data(a, b):
    x, y = some_data(a, b, file_name=f"my_data_{a}_{b}")

    f = plt.figure(figsize=[6.5, 4])
    plt.plot(x, y)

    return f


def main():
    plot_some_data(5, 8, file_name="my_plot")


nh.run(main)
```

Similar approaches are possible with many other frameworks (for example with
plain numpy or [jupyter notebooks](https://github.com/jupyter/notebook)). The
main strength of this frameworks is its simplicity and that it results in a
deterministic script that can be rerun many times, producing the same result
every time fast. This is especially useful if figures are replotted a long time
after writing the script - perhaps just to change a simple detail.

## Documentation

See [uduh.gitlab.io/numpy_helper/numpy_helper.html](https://uduh.gitlab.io/numpy_helper/numpy_helper.html)
for pdoc generated pages.

## Roadmap

The project is in a usable state. It's mostly intended for personal use, but
other users and merge requests are welcome. If there is interest (please star
the repository or [email](mailto:urban.duh@protonmail.com) me), the project will
be published on PyPI.

