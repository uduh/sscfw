let
  pkgs = import <nixpkgs> { };
in
pkgs.mkShell {
  name = "nh-shell";
  buildInputs = [
    (pkgs.python3.withPackages (pp: with pp;
    [
      numpy
      matplotlib
      pdoc
    ]))
  ];
}
