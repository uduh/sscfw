"""
A general usage example of numpy_helper.
"""

import numpy_helper as nh
import numpy as np
import matplotlib.pyplot as plt


@nh.cache_numpys(num_of_returns=2)
def some_data(a, b=2):
    x = np.linspace(0, 2 * np.pi, 1000)
    y = b * np.sin(a * x)

    return x, y


@nh.handle_plot
def plot_some_data(a, b):
    x, y = some_data(a, b, file_name=f"my_data_{a}_{b}")

    f = plt.figure(figsize=[6.5, 4])
    plt.plot(x, y)

    return f


def main():
    plot_some_data(5, 8, file_name="my_plot")


nh.run(main)
