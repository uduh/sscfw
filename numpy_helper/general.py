"""
General functions for plotting and caching.
See usage example in `numpy_helper` documentation.
"""

from __future__ import annotations
import argparse
from collections.abc import Callable
import os
import time
import zlib

import matplotlib.pyplot as plt
import numpy as np

export = False
"""
@private
"""
recompute = False
"""
@private
"""


def cache_numpys(num_of_returns: int) -> Callable:
    """
    Cache numpy arrays returned by a function in a file.
    Use as a decorator of a function that returns a tuple of numpy ndarrays.

    :param num_of_returns (int): Length of the returned tuple.

    Decorated function supports new arguments:
    - **file_name (str)**: used name of file to export to. If equal to `""` (default),
    file name is a hash calculated from argument values.
    - **data_path (str)**: directory of the exported file. Defaults to `"data"`.

    The decorated function must be called within the `run` function.
    If script is called with `-r` flag, the data is recomputed even if already cached before.
    See usage example in `nh`.
    """

    def inner_cache_numpys(func: Callable) -> Callable:
        def caching_func(
            *args,
            file_name: str = "",
            data_path: str = "data",
            adler32_seed: int = 1,
            **kwargs,
        ):
            # Create given data path if it doesn't exists.
            if not os.path.exists(data_path):
                os.makedirs(data_path)

            # Construct file name if not given by the user.
            if file_name == "":
                file_name = str(
                    zlib.adler32(bytes(str(args), "utf-8"), adler32_seed)
                    + zlib.adler32(bytes(str(kwargs), "utf-8"), adler32_seed)
                )

            results = []

            # Check if the result is already cached, otherwise calculate and cache it.
            if not recompute and os.path.exists(
                os.path.join(
                    data_path, "{}_{}.npy".format(file_name, num_of_returns - 1)
                )
            ):
                for i in range(num_of_returns):
                    file_path = os.path.join(
                        data_path, "{}_{}.npy".format(file_name, i)
                    )
                    results.append(np.load(file_path))
            else:
                results = func(*args, **kwargs)
                for i, result in enumerate(results):
                    file_path = os.path.join(
                        data_path, "{}_{}.npy".format(file_name, i)
                    )
                    np.save(file_path, result)

            return results

        return caching_func

    return inner_cache_numpys


def cache_numpy(func: Callable) -> Callable:
    """
    Convenience function similar to `cache_numpys`, but for functions returning only a single
    numpy ndarray (not a tuple).
    """
    multi_dim_func = cache_numpys(1)(lambda *args, **kwargs: (func(*args, **kwargs),))
    return lambda *args, **kwargs: multi_dim_func(*args, **kwargs)[0]


def handle_plots(func: Callable) -> Callable:
    """
    Export the plots if export flag is given, otherwise show them.
    Use as a decorator of a function that returns a tuple of pyplot Figure objects.

    Decorated function supports new arguments:
    - **file_name (str)**: used name of file to export to. If not set, exporting
    raises an exception.
    - **image_path (str)**: location of the exported file. Defaults to `"images"`.
    - **file_extension (str)**:  extension of the exported file. Defaults to `".pdf"`.
    - **dpi (int)**: delegated to `matplotlib.pyplot.savefig`. Use `None` (default) for
    the default pyplot value.

    The decorated function must be called within the `run` function.
    If script is called with `-e` flag, the figure is exported to the defined file.
    Otherwise, all the figures are shown when the function called in `run` returns.
    See usage example in `numpy_helper`.
    """

    def handled_func(
        *args,
        file_name: str = "",
        file_extension: str = ".pdf",
        image_path: str = "images",
        dpi: int | None = None,
        **kwargs,
    ):
        if export and file_name == "":
            raise Exception(
                "Cannot not export to a file without name, provide the file_name argument."
            )

        # Create given data path if it doesn't exists.
        if export and not os.path.exists(image_path):
            os.makedirs(image_path)

        figs = func(*args, **kwargs)

        if len(figs) == 1:
            fig = figs[0]
            if export:
                file_path = os.path.join(image_path, file_name + file_extension)
                if dpi is not None:
                    fig.savefig(file_path, bbox_inches="tight", dpi=dpi)
                else:
                    fig.savefig(file_path, bbox_inches="tight")
            else:
                fig.show()
        else:
            for i, fig in enumerate(figs):
                if export:
                    file_path = os.path.join(
                        image_path, f"{file_name}_{i + 1}" + file_extension
                    )
                    if dpi is not None:
                        fig.savefig(file_path, bbox_inches="tight", dpi=dpi)
                    else:
                        fig.savefig(file_path, bbox_inches="tight")
                else:
                    fig.show()

    return handled_func


def handle_plot(func: Callable) -> Callable:
    """
    Convenience function similar to `handle_plots`, but for functions returning only a single
    Figure object (and not a tuple).
    """
    return handle_plots(lambda *args, **kwargs: (func(*args, **kwargs),))


def run(func: Callable, force_export: bool = False, force_recompute: bool = False):
    """
    Execute the given function `func` and correctly handle all `cache_numpys` and
    `handle_plots` decorated functions. Displays elapsed time at the end.
    See usage example in `numpy_helper`.

    :param func: The function to execute.
    :param force_export (bool): Use the `-e` flag by default (see `handle_plots`).
    :param force_recompute (bool): Use the `-r` flag by default (see `cache_numpys`).
    """
    # Init argument parser.
    parser = argparse.ArgumentParser(
        description="Script using the Numpy Helper library."
    )
    parser.add_argument(
        "-e", "--export", action="store_true", help="export all figures to files"
    )
    parser.add_argument(
        "-r", "--recompute", action="store_true", help="recompute all cached data"
    )
    args = parser.parse_args()

    # Set global variables.
    global export
    if args.export or force_export:
        export = True

    global recompute
    if args.recompute or force_recompute:
        recompute = True

    # Time and run function.
    begin = time.perf_counter()
    func()
    end = time.perf_counter()

    print("\n\nElapsed time: " + str(end - begin) + " s")
    if not export:
        plt.show()  # Block execution to view figures.

    # Reset global variables.
    export = False
    recompute = False
