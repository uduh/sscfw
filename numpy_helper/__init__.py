"""
.. include:: ../README.md
"""

from .general import cache_numpy, cache_numpys, handle_plot, handle_plots, run
